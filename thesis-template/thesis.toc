\selectlanguage *{english}
\renewcommand {\@dotsep }{400}
\contentsline {chapter}{ACKNOWLEDGMENTS}{ii}{section*.1}%
\renewcommand {\@dotsep }{0.5}
\renewcommand {\@dotsep }{400}
\contentsline {chapter}{ABSTRACT}{iii}{section*.2}%
\renewcommand {\@dotsep }{0.5}
\thispagestyle {fancy}
\fancyhead [L]{}
\fancyhead [R]{\thepage }
\renewcommand {\@dotsep }{400}
\contentsline {chapter}{LIST OF TABLES}{vii}{section*.5}%
\renewcommand {\@dotsep }{0.5}
\renewcommand {\@dotsep }{400}
\contentsline {chapter}{LIST OF FIGURES}{viii}{section*.7}%
\renewcommand {\@dotsep }{0.5}
\renewcommand {\@dotsep }{400}
\contentsline {chapter}{LIST OF LISTINGS}{ix}{section*.9}%
\renewcommand {\@dotsep }{0.5}
\vspace *{0.7\baselineskip }
\contentsline {chapter}{\nocolonnumberline {\bfseries 1}\bfseries \MakeUppercase {Introduction}}{1}{chapter.1}%
\contentsline {section}{\nocolonnumberline {1.1}\MakeUppercase {Motivation}}{1}{section.1.1}%
\contentsline {section}{\nocolonnumberline {1.2}\MakeUppercase {Problem Statement}}{1}{section.1.2}%
\contentsline {section}{\nocolonnumberline {1.3}\MakeUppercase {Objective of project}}{1}{section.1.3}%
\contentsline {section}{\nocolonnumberline {1.4}\MakeUppercase {scope of the project}}{1}{section.1.4}%
\contentsline {chapter}{\nocolonnumberline {\bfseries 2}\bfseries \MakeUppercase {Background}}{2}{chapter.2}%
\contentsline {section}{\nocolonnumberline {2.1}\MakeUppercase {Your section}}{2}{section.2.1}%
\contentsline {chapter}{\nocolonnumberline {\bfseries 3}\bfseries \MakeUppercase {Analysis and Design}}{3}{chapter.3}%
\contentsline {section}{\nocolonnumberline {3.1}\MakeUppercase {System Architecture Overview}}{3}{section.3.1}%
\contentsline {chapter}{\nocolonnumberline {\bfseries 4}\bfseries \MakeUppercase {Implementation}}{4}{chapter.4}%
\contentsline {section}{\nocolonnumberline {4.1}\MakeUppercase {System Environment}}{4}{section.4.1}%
\contentsline {chapter}{\nocolonnumberline {\bfseries 5}\bfseries \MakeUppercase {Testing and Evaluation}}{5}{chapter.5}%
\contentsline {section}{\nocolonnumberline {5.1}\MakeUppercase {Your section}}{5}{section.5.1}%
\contentsline {chapter}{\nocolonnumberline {\bfseries 6}\bfseries \MakeUppercase {Related works}}{6}{chapter.6}%
\contentsline {section}{\nocolonnumberline {6.1}\MakeUppercase {Your section}}{6}{section.6.1}%
\contentsline {chapter}{\nocolonnumberline {\bfseries 7}\bfseries \MakeUppercase {Discussion}}{7}{chapter.7}%
\contentsline {section}{\nocolonnumberline {7.1}\MakeUppercase {Your section}}{7}{section.7.1}%
\contentsline {chapter}{\nocolonnumberline {\bfseries 8}\bfseries \MakeUppercase {Conclusions}}{8}{chapter.8}%
\contentsline {section}{\nocolonnumberline {8.1}\MakeUppercase {Your section}}{8}{section.8.1}%
\setcounter {tocdepth}{0}
\renewcommand {\@dotsep }{400}
\contentsline {chapter}{APPENDIX A}{9}{appendix.1}%
\setcounter {tocdepth}{0}
\renewcommand {\@dotsep }{400}
\contentsline {chapter}{APPENDIX B}{10}{appendix.2}%
\setcounter {tocdepth}{0}
\renewcommand {\@dotsep }{400}
\contentsline {chapter}{APPENDIX C}{11}{appendix.3}%
\setcounter {tocdepth}{0}
\renewcommand {\@dotsep }{400}
\contentsline {chapter}{REFERENCES}{12}{appendix.3}%
\setcounter {tocdepth}{0}
\renewcommand {\@dotsep }{400}
\contentsline {chapter}{BIOGRAPHIES}{13}{appendix.3}%
