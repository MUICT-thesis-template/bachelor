\selectlanguage *{english}
\renewcommand {\@dotsep }{400}
\contentsline {chapter}{ACKNOWLEDGMENTS}{ii}{section*.1}%
\renewcommand {\@dotsep }{0.5}
\renewcommand {\@dotsep }{400}
\contentsline {chapter}{ABSTRACT}{iii}{section*.2}%
\renewcommand {\@dotsep }{0.5}
\thispagestyle {fancy}
\fancyhead [L]{}
\fancyhead [R]{\thepage }
\renewcommand {\@dotsep }{400}
\contentsline {chapter}{LIST OF TABLES}{vii}{section*.5}%
\renewcommand {\@dotsep }{0.5}
\renewcommand {\@dotsep }{400}
\contentsline {chapter}{LIST OF FIGURES}{viii}{section*.7}%
\renewcommand {\@dotsep }{0.5}
\renewcommand {\@dotsep }{400}
\contentsline {chapter}{LIST OF LISTINGS}{ix}{section*.9}%
\renewcommand {\@dotsep }{0.5}
\vspace *{0.7\baselineskip }
\contentsline {chapter}{\nocolonnumberline {\bfseries 1}\bfseries \MakeUppercase {How to use \LaTeX \nobreakspace {} for your thesis?}}{1}{chapter.1}%
\contentsline {section}{\nocolonnumberline {1.1}\MakeUppercase {Whait is \LaTeX ?}}{1}{section.1.1}%
\contentsline {section}{\nocolonnumberline {1.2}\MakeUppercase {\TeX \nobreakspace {} structure}}{1}{section.1.2}%
\contentsline {section}{\nocolonnumberline {1.3}\MakeUppercase {Switching the language}}{2}{section.1.3}%
\contentsline {section}{\nocolonnumberline {1.4}\MakeUppercase {Generating ICT thesis}}{2}{section.1.4}%
\contentsline {section}{\nocolonnumberline {1.5}\MakeUppercase {Using thesis.tex}}{3}{section.1.5}%
\contentsline {section}{\nocolonnumberline {1.6}\MakeUppercase {Generating the page styles}}{6}{section.1.6}%
\contentsline {chapter}{\nocolonnumberline {\bfseries 2}\bfseries \MakeUppercase {\LaTeX 's\nobreakspace {}Arsenal}}{8}{chapter.2}%
\contentsline {section}{\nocolonnumberline {2.1}\MakeUppercase {Sectioning}}{8}{section.2.1}%
\contentsline {section}{\nocolonnumberline {2.2}\MakeUppercase {Paragraph}}{9}{section.2.2}%
\contentsline {section}{\nocolonnumberline {2.3}\MakeUppercase {Paragraph alignment}}{10}{section.2.3}%
\contentsline {subsection}{\nocolonnumberline {2.3.1}\MakeUppercase {Indentation}}{10}{subsection.2.3.1}%
\contentsline {subsection}{\nocolonnumberline {2.3.2}\MakeUppercase {Format alignment}}{11}{subsection.2.3.2}%
\contentsline {section}{\nocolonnumberline {2.4}\MakeUppercase {Bullets and Numbering}}{11}{section.2.4}%
\contentsline {section}{\nocolonnumberline {2.5}\MakeUppercase {Figure}}{13}{section.2.5}%
\contentsline {section}{\nocolonnumberline {2.6}\MakeUppercase {Table}}{15}{section.2.6}%
\contentsline {section}{\nocolonnumberline {2.7}\MakeUppercase {Page sectioning}}{17}{section.2.7}%
\contentsline {section}{\nocolonnumberline {2.8}\MakeUppercase {Landscape mode}}{18}{section.2.8}%
\contentsline {section}{\nocolonnumberline {2.9}\MakeUppercase {Text style, type, and size}}{20}{section.2.9}%
\contentsline {subsection}{\nocolonnumberline {2.9.1}\MakeUppercase {Text style}}{20}{subsection.2.9.1}%
\contentsline {subsection}{\nocolonnumberline {2.9.2}\MakeUppercase {Text size}}{20}{subsection.2.9.2}%
\contentsline {section}{\nocolonnumberline {2.10}\MakeUppercase {Mathematic Formular}}{21}{section.2.10}%
\contentsline {subsection}{\nocolonnumberline {2.10.1}\MakeUppercase {Numbering the multiple equations}}{22}{subsection.2.10.1}%
\contentsline {subsection}{\nocolonnumberline {2.10.2}\MakeUppercase {Subscript and Superscript}}{23}{subsection.2.10.2}%
\contentsline {subsection}{\nocolonnumberline {2.10.3}\MakeUppercase {Spacing in math mode}}{23}{subsection.2.10.3}%
\contentsline {subsection}{\nocolonnumberline {2.10.4}\MakeUppercase {Adjust delimiter size}}{24}{subsection.2.10.4}%
\contentsline {section}{\nocolonnumberline {2.11}\MakeUppercase {Adding footnotes}}{25}{section.2.11}%
\contentsline {chapter}{\nocolonnumberline {\bfseries 3}\bfseries \MakeUppercase {Writing bib extension file}}{27}{chapter.3}%
\setcounter {tocdepth}{0}
\renewcommand {\@dotsep }{400}
\contentsline {chapter}{APPENDIX A}{30}{appendix.1}%
\contentsline {section}{\nocolonnumberline {A.1}\MakeUppercase {Symbol}}{30}{section.3.1}%
\contentsline {section}{\nocolonnumberline {A.2}\MakeUppercase {Font}}{33}{section.3.2}%
\setcounter {tocdepth}{0}
\renewcommand {\@dotsep }{400}
\contentsline {chapter}{APPENDIX B}{34}{appendix.2}%
\contentsline {section}{\nocolonnumberline {B.1}\MakeUppercase {Article}}{34}{section.3.1}%
\contentsline {section}{\nocolonnumberline {B.2}\MakeUppercase {Unpublished document}}{34}{section.3.2}%
\contentsline {section}{\nocolonnumberline {B.3}\MakeUppercase {Inproceeding paper}}{34}{section.3.3}%
\contentsline {section}{\nocolonnumberline {B.4}\MakeUppercase {Proceeding paper}}{35}{section.3.4}%
\contentsline {section}{\nocolonnumberline {B.5}\MakeUppercase {Technical report}}{36}{section.3.5}%
\contentsline {section}{\nocolonnumberline {B.6}\MakeUppercase {Website}}{36}{section.3.6}%
\contentsline {section}{\nocolonnumberline {B.7}\MakeUppercase {PhD. and Master's thesis}}{37}{section.3.7}%
\contentsline {section}{\nocolonnumberline {B.8}\MakeUppercase {Book}}{37}{section.3.8}%
\contentsline {section}{\nocolonnumberline {B.9}\MakeUppercase {Miscelaneous}}{38}{section.3.9}%
\setcounter {tocdepth}{0}
\renewcommand {\@dotsep }{400}
\contentsline {chapter}{REFERENCES}{39}{lstnumber.3.9.10}%
\setcounter {tocdepth}{0}
\renewcommand {\@dotsep }{400}
\contentsline {chapter}{BIOGRAPHIES}{41}{chapter*.61}%
