\contentsline {lstlisting}{\numberline {1.1}\TeX \nobreakspace {}structure}{1}{lstlisting.1.1}%
\contentsline {lstlisting}{\numberline {1.2}\TeX \nobreakspace {}file header}{3}{lstlisting.1.2}%
\contentsline {lstlisting}{\numberline {1.3}Author's information}{4}{lstlisting.1.3}%
\contentsline {lstlisting}{\numberline {1.4}Advisor's information}{5}{lstlisting.1.4}%
\contentsline {lstlisting}{\numberline {1.5}Thesis's information}{5}{lstlisting.1.5}%
\contentsline {lstlisting}{\numberline {1.6}Page generator command}{6}{lstlisting.1.6}%
\contentsline {lstlisting}{\numberline {1.7}Call the external file via \textbackslash input}{7}{lstlisting.1.7}%
\contentsline {lstlisting}{\numberline {2.1}Sectioning}{8}{lstlisting.2.1}%
\contentsline {lstlisting}{\numberline {2.2}Extra spaces between paragraphs}{9}{lstlisting.2.2}%
\contentsline {lstlisting}{\numberline {2.3}Indent spaces}{10}{lstlisting.2.3}%
\contentsline {lstlisting}{\numberline {2.4}Using itemize and enumerate}{12}{lstlisting.2.4}%
\contentsline {lstlisting}{\numberline {2.5}Using itemize and enumerate}{13}{lstlisting.2.5}%
\contentsline {lstlisting}{\numberline {2.6}The figure environment structure}{13}{lstlisting.2.6}%
\contentsline {lstlisting}{\numberline {2.7}Using subcaption package}{14}{lstlisting.2.7}%
\contentsline {lstlisting}{\numberline {2.8}Create the table}{16}{lstlisting.2.8}%
\contentsline {lstlisting}{\numberline {2.9}Minipage example}{17}{lstlisting.2.9}%
\contentsline {lstlisting}{\numberline {2.10}Enable landscape mode}{18}{lstlisting.2.10}%
\contentsline {lstlisting}{\numberline {2.11}Command for setting font environment}{20}{lstlisting.2.11}%
\contentsline {lstlisting}{\numberline {2.12}Formular environment declaration}{21}{lstlisting.2.12}%
\contentsline {lstlisting}{\numberline {2.13}Multiple equations}{22}{lstlisting.2.13}%
\contentsline {lstlisting}{\numberline {2.14}Create blank space in math environment}{24}{lstlisting.2.14}%
\contentsline {lstlisting}{\numberline {2.15}The differences of delimiter size when using and not using the \textbackslash left \textbackslash right command.}{25}{lstlisting.2.15}%
\contentsline {lstlisting}{\numberline {2.16}Adding footnote}{25}{lstlisting.2.16}%
\contentsline {lstlisting}{\numberline {3.1}Define the reference format}{27}{lstlisting.3.1}%
\contentsline {lstlisting}{\numberline {3.2}BibTex data}{28}{lstlisting.3.2}%
\contentsline {lstlisting}{\numberline {B.1}Article}{34}{lstlisting.3.1}%
\contentsline {lstlisting}{\numberline {B.2}Unpublished document}{34}{lstlisting.3.2}%
\contentsline {lstlisting}{\numberline {B.3}Inproceeding paper}{35}{lstlisting.3.3}%
\contentsline {lstlisting}{\numberline {B.4}Proceeding paper}{35}{lstlisting.3.4}%
\contentsline {lstlisting}{\numberline {B.5}Technical report}{36}{lstlisting.3.5}%
\contentsline {lstlisting}{\numberline {B.6}Website}{36}{lstlisting.3.6}%
\contentsline {lstlisting}{\numberline {B.7}PhD. and Master's thesis}{37}{lstlisting.3.7}%
\contentsline {lstlisting}{\numberline {B.8}Book}{37}{lstlisting.3.8}%
\contentsline {lstlisting}{\numberline {B.9}Miscellaneous}{38}{lstlisting.3.9}%
